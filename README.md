<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# Book Catalog App

This Laravel application allows users to manage a book catalog by adding, editing, viewing, and deleting books. Users can easily organize and keep track of their book collection using this web application.

## Features

- **Add Books**: Users can add new books to their catalog by providing details such as title, author, description, and cover image.

- **Edit Books**: Users can update the information of existing books in the catalog, including modifying the title, author, description, or cover image.

- **View Books**: Users can browse and view the books in their catalog, displaying key information such as the title, author, and cover image.

- **Delete Books**: Users have the option to remove unwanted books from their catalog, ensuring an organized collection.

- **Third-Party Integration**: The application integrates with the Google Books API to fetch additional details about the books. This integration enhances the catalog with comprehensive book information, including summaries, ratings, and more.

## Technologies Used

This Laravel application utilizes the following technologies:

- Laravel Framework: A powerful PHP framework that provides a robust foundation for building web applications.

- Livewire: Livewire is a full-stack framework for Laravel that makes building dynamic interfaces simple, without leaving the comfort of Laravel.

- MySQL Database: The database management system used to store and retrieve book catalog information.

- Google Books API: A third-party service used to fetch detailed book information, enriching the catalog with additional data.

## Installation and Setup

To set up and run this Laravel application locally, follow the instructions below:

1. Clone the repository to your local machine.

2. Configure the `.env` file with the necessary database connection details.

3. Install the project dependencies using Composer:
