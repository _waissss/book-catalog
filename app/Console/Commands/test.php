<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // $response = Http::get('https://www.googleapis.com/books/v1/volumes', [
        //     'q' => 'Harry Potter',
        //     'fields' => 'items(volumeInfo/industryIdentifiers)',
        //     'key' => config('services.google.api_key')
        // ]);

        // dd($response->json()['items']);

        // $response = Http::get('https://www.googleapis.com/books/v1/volumes', [
        //     'q' => 'isbn:979888663045',
        //     'key' => config('services.google.api_key')
        // ]);

        $response = Http::get('https://www.googleapis.com/books/v1/volumes', [
            'q' => 'isbn:0470398256' ,
            'key' => config('services.google.api_key')
        ]);

        dd($response->json());

    }
}
