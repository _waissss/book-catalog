<?php

namespace App\Http\Livewire;

use App\Models\Book;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Livewire\Component;

class AddBook extends Component
{
    public $title, $author, $publication_year, $genre, $isbn;

    public function render()
    {
        return view('livewire.add-book');
    }

    public function submit()
    {
        $this->validate([
            'title' => 'required',
            'author' => 'required',
            'publication_year' => 'required|numeric',
            'genre' => 'required',
            'isbn' => 'required'
        ]);

        $response = Http::get('https://www.googleapis.com/books/v1/volumes', [
            'q' => 'isbn:' . $this->isbn,
            'key' => config('services.google.api_key')
        ]);

        if (isset($response->json()['items'])) {
            $bookData = $response->json()['items'][0]['volumeInfo'];

            Book::create([
                'title' => $bookData['title'] ?? $this->title,
                'author' => $bookData['authors'][0] ?? $this->author,
                'publication_year' => $this->publication_year,
                'genre' => $this->genre,
                'isbn' => $this->isbn,
                'user_id' => auth()->id()
            ]);

            $this->emit('updateBookDatatable');
        } else {
            $this->addError('isbn', 'ISBN number is not valid, please get the ISBN number in Search ISBN number menu at navigation bar');
        }
    }
}
