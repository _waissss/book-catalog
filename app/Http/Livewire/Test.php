<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Test extends Component
{
    public $counter;

    public function count()
    {
        $this->counter++;
    }
    
    public function render()
    {
        return view('livewire.test');
    }
}
