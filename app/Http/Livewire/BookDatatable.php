<?php

namespace App\Http\Livewire;

use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Book;
use Illuminate\Database\Eloquent\Builder;

class BookDatatable extends DataTableComponent
{
    protected $model = Book::class;

    protected $listeners = ['updateBookDatatable', 'deleteBook'];

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setTableAttributes([
            'class' => 'table table-hover table-rounded gy-4 gx-4',
        ]);
        $this->setTheadAttributes([
            'class' => 'fw-bold fs-6',
        ]);
    }

    public function columns(): array
    {
        return [
            Column::make("Id", "id")
                ->sortable(),
            Column::make("Title", "title")->format(function ($value, $row) {
                $html = '<a href="' . route('book.show', $row->id) . '">' . $value . '</a>';

                return $html;
            })->html()
                ->sortable()->searchable(),
            Column::make("Author", "author")
                ->sortable()->searchable(),
            Column::make("Publication Year", "publication_year")
                ->sortable()->searchable(),
            Column::make("Created at", "created_at")
                ->sortable(),
            Column::make("Action")->label(function ($row) {

                $html = '<a href="javascript:void;" class="btn btn-icon btn-success" wire:click="editBook(' . $row->id . ')">Edit</a>';
                $html .= '<a href="javascript:void;" class="btn btn-icon btn-danger" wire:click="confirmDelete(' . $row->id . ')">Delete</a>';

                return $html;
            })->html()

        ];
    }

    public function builder(): Builder
    {
        return Book::select('books.*');
    }

    public function confirmDelete($id)
    {
        $this->dispatchBrowserEvent('swal:confirm', [
            'type' => 'question',
            'title' => 'file',
            'text' => 'Are you sure you want to delete this ?',
            'id' => $id
        ]);
    }

    public function editBook($id)
    {
        $this->dispatchBrowserEvent('open-x-modal', [
            'title' => 'Edit Book',
            'modal' => 'edit-book',
            'size' => 'lg',
            'args' => ['id' => $id]
        ]);
    }

    public function updateBookDatatable()
    {
        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Great!',
            'text' => 'Book Added/Updated!'
        ]);

        $this->dispatchBrowserEvent('closeLivewireModal');
    }

    public function deleteBook($id)
    {
        $book = Book::find($id);
        $book->delete();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Great!',
            'text' => 'Book Deleted!'
        ]);

        $this->dispatchBrowserEvent('closeLivewireModal');
    }
}
