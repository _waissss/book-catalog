<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BookController extends Controller
{
    public function index()
    {
        $searchTerm = request('searchTerm');

        $response = Http::get('https://www.googleapis.com/books/v1/volumes', [
            'q' => $searchTerm ?? '',
            'fields' => 'items(volumeInfo/industryIdentifiers)',
            'key' => config('services.google.api_key')
        ]);

        if (isset($response->json()['items'])) {
            $datas = collect($response->json()['items']);
        } else {
            $datas = collect();
        }

        return view('search_isbn', compact('datas'));
    }

    public function show(Book $book)
    {
        $response = Http::get('https://www.googleapis.com/books/v1/volumes', [
            'q' => 'isbn:' . $book->isbn,
            'key' => config('services.google.api_key')
        ]);

        // Get the response body as JSON
        $data = $response->json();

        $description = 'No Description available for this book.';
        $previewLink = 'No Preview Link available for this book.';

        if (isset($data['items'])) {
            $books = $data['items'][0]; // Retrieve the first book

            $description = $books['volumeInfo']['description'] ?? 'No Description Found for this book';
            $previewLink = $books['volumeInfo']['previewLink'] ?? 'No Preview Link Found for this book';
        }

        return view('book_detail', compact('book', 'description', 'previewLink'));
    }

    public function storeReview(Request $request, Book $book)
    {
        $request->validate([
            'content' => 'required|max:500',
        ]);

        $book->reviews()->create([
            'user_id' => auth()->id(),
            'content' => $request->content,
        ]);

        return redirect()->back()->with('message', 'Review added successfully');
    }
}
