<?php

use App\Http\Controllers\BookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth'])->group(function () {
    Route::get('search-isbn', [BookController::class, 'index'])->name('book.index');
    Route::get('book/show/{book}', [BookController::class, 'show'])->name('book.show');
    Route::post('/books/{book}/reviews', [BookController::class, 'storeReview'])->name('books.reviews.store')->middleware('auth');
});
