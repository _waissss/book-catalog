@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">

                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h5 class="m-2">{{ __('Search For Any Book') }}</h5>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="mb-3">
                            <form action="{{ route('book.index') }}" method="GET">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="searchTerm" placeholder="Search Books" value="{{ request('searchTerm') }}">
                                    <button class="btn btn-primary" type="submit">Search</button>
                                </div>
                            </form>
                        </div>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">ISBN Number</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($datas as $key => $data)
                                    @foreach($data['volumeInfo']['industryIdentifiers'] as $vol)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $vol['identifier'] }}</td>
                                    </tr>
                                    @endforeach
                                @empty
                                    <tr>
                                        <td>No Result Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
