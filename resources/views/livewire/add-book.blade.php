<x-livewiremodal-modal>
    <!--begin::Input group-->
    <div class="row mb-5">
        <!--begin::Input group-->
        <div class="row mb-4">
            <!--begin::Label-->
            <label class="col-lg-3 col-form-label fw-bold fs-6">Title<span style="color:red;">*</span></label>
            <!--end::Label-->
            <!--begin::Col-->
            <div class="col-lg-9">
                <input type="text" wire:model.defer="title" class="form-control">
                @error('title')
                    <div class="text-danger mt-2">{{ $message }}</div>
                @enderror
            </div>
            <!--end::Col-->
        </div>
        <!--end::Input group-->
        <!--begin::Input group-->
        <div class="row mb-4">
            <!--begin::Label-->
            <label class="col-lg-3 col-form-label fw-bold fs-6">Author<span style="color:red;">*</span></label>
            <!--end::Label-->
            <!--begin::Col-->
            <div class="col-lg-9">
                <input type="text" wire:model.defer="author" class="form-control">
                @error('author')
                    <div class="text-danger mt-2">{{ $message }}</div>
                @enderror
            </div>
            <!--end::Col-->
        </div>
        <!--end::Input group-->
        <!--begin::Input group-->
        <div class="mb-4 row">
            <!--begin::Label-->
            <label class="col-lg-3 col-form-label fw-bold fs-6">Publication Year<span style="color:red;">*</span></label>
            <!--end::Label-->
            <!--begin::Col-->
            <div class="col-lg-9">
                <input type="text" wire:model.defer="publication_year" class="form-control">
                @error('publication_year')
                    <div class="text-danger mt-2">{{ $message }}</div>
                @enderror
            </div>
            <!--end::Col-->
        </div>
        <!--end::Input group-->
        <!--begin::Input group-->
        <div class="mb-4 row">
            <!--begin::Label-->
            <label class="col-lg-3 col-form-label fw-bold fs-6">Genre<span style="color:red;">*</span></label>
            <!--end::Label-->
            <!--begin::Col-->
            <div class="col-lg-9">
                <input type="text" wire:model.defer="genre" class="form-control">
                @error('genre')
                    <div class="text-danger mt-2">{{ $message }}</div>
                @enderror
            </div>
            <!--end::Col-->
        </div>
        <!--end::Input group-->
        <!--begin::Input group-->
        <div class="mb-4 row">
            <!--begin::Label-->
            <label class="col-lg-3 col-form-label fw-bold fs-6">ISBN<span style="color:red;">*</span></label>
            <!--end::Label-->
            <!--begin::Col-->
            <div class="col-lg-9">
                <input type="text" wire:model.defer="isbn" class="form-control">
                @error('isbn')
                    <div class="text-danger mt-2">{{ $message }}</div>
                @enderror
            </div>
            <!--end::Col-->
        </div>
        <!--end::Input group-->
    </div>
    <!--begin::Actions-->
    <x-slot name="footer">
        <button class="btn btn-primary" wire:click.prevent="submit">Submit</button>
    </x-slot>
    <!--end::Actions-->
</x-livewiremodal-modal>
