<div>
    <div class="card">
        <div class="card-header">{{ __('Dashboard') }}</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

          {{ $counter }}
        </div>

        <button class="btn btn-primary" wire:click.prevent="count">Counter</button>
    </div>
</div>
