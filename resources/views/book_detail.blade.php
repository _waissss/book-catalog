@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{ $book->title }}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">{{ $book->author }}</h6>
                        <p class="card-text">Published in {{ $book->publication_year }}</p>
                        <p class="card-text">Genre: {{ $book->genre }}</p>
                        <p class="card-text">ISBN: {{ $book->isbn }}</p>
                        <p class="card-text">Description:</p>
                        <p class="card-text">{{ $description }}</p>
                        <p class="card-text">Preview Link:</p>
                        @if (!empty($previewLink))
                            <p class="card-text"><a href="{{ $previewLink }}" target="_blank">{{ $previewLink }}</a></p>
                        @else
                            <p class="card-text">{{ $previewLink }}</p>
                        @endif
                    </div>
                </div>

                <!-- Reviews -->
                <div class="mt-4">
                    <h3>Reviews</h3>
                    @foreach ($book->reviews as $review)
                        <div class="card mb-3">
                            <div class="card-body">
                                <h5 class="card-title">{{ $review->user->name }}</h5>
                                <p class="card-text">{{ $review->content }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>

                <!-- Review form -->
                @if (\Auth::check())
                    <div class="mt-4">
                        <h3>Submit a review</h3>
                        <form method="POST" action="{{ url('/books/' . $book->id . '/reviews') }}">
                            @csrf
                            <div class="form-group">
                                <label for="review-content">Your review</label>
                                <textarea class="form-control" id="review-content" name="content"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary mt-2">Submit</button>
                        </form>
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection
