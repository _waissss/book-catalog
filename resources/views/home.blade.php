@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">

                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h5 class="m-2">{{ __('Book List') }}</h5>
                        <x-livewiremodal-trigger class="btn btn-primary" title="Add Book" modal="add-book" :args="[]" lg>Add Book
                        </x-livewiremodal-trigger>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @livewire('book-datatable')
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
